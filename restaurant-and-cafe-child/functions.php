<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
    
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}
function restaurant_and_cafe_widgets_init_1() {
    register_sidebar( array(
        'name'          => esc_html__( 'Header Widget One', 'restaurant-and-cafe' ),
        'id'            => 'header-widget-one',
        'description'   => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'restaurant_and_cafe_widgets_init_1' );
